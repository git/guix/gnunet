;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-container-metadata)
  #:use-module (srfi srfi-64)
  #:use-module (rnrs bytevectors)
  #:use-module (gnu gnunet container metadata))

(test-begin "test-container-metadata")

(define test-meta (make-metadata))
(define test-data1 (string->utf8 "foo.scm"))
(define test-item1
  (make-metadata-item "foo" #:original-filename #:utf8 "text/plain" test-data1))
(define test-data2 (string->utf8 "bar.txt"))
(define test-item2
  (make-metadata-item "bar" #:unknown #:utf8 "text/plain" test-data2))

(metadata-set! test-meta test-item1)
(metadata-set! test-meta test-item2)

(test-equal "foo.scm" (metadata-ref test-meta #:original-filename))

(let ((lst '()))
  (metadata-iterate (lambda (name . _)
		      (set! lst (cons name lst)))
		    test-meta)
  (test-assert (or (equal? '("foo" "bar") lst)
		   (equal? '("bar" "foo") lst))))

(test-equal '("foo" "bar")
	    (metadata-map (lambda (name . _) name) test-meta))

;; copy
(define test-meta-copy (metadata-copy test-meta))
(test-equal "foo.scm" (metadata-ref test-meta-copy #:original-filename))

;; equal?
(test-assert (metadata-equal? test-meta test-meta-copy))

;; clear!
(metadata-clear! test-meta-copy)
(test-equal #f (metadata-ref test-meta-copy #:original-filename))

;; add-publication-date!
(metadata-add-publication-date! test-meta-copy)
(test-assert (metadata-ref test-meta-copy #:publication-date))

(test-end)
