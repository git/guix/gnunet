;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-fs-progress-info)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)
  #:use-module (system foreign unions)
  #:use-module (gnu gnunet common)
  #:use-module (gnu gnunet container metadata)
  #:use-module (gnu gnunet fs uri)
  #:use-module (gnu gnunet fs progress-info))


(define-syntax-rule (pi-import name ...)
  (begin (define name (@@ (gnu gnunet fs progress-info) name))
         ...))

(pi-import integer->progress-info-status
           progress-info-status->integer
           bytevector-u8-fold
           u8-bitmap->list
           %progress-info-type)

(define *test-uri*
  (parse-uri "gnunet://fs/chk/AH11VENCEPEH119B1TQQ06CT170TA400J653E9G2D7JPV57HRN528KK71270D81PAV23GBNNPS6KKQM48C1H7FG41JT1ETPK551MRH8.74DJF0M1T999MC6K65NV1MC0RG11S81127JS9SV1M79QE2S6GMSQE0K87110D95J9HV0VDCGFG11BK97C2E5BD2T5F6TQTAFF6KP3F0.50"))

(define *test-pinfo-ptr*
  (make-c-struct* %progress-info-type
                  (list (list %null-pointer                ; context
                              %null-pointer                ; cctx
                              %null-pointer                ; pctx
                              %null-pointer                ; sctx
                              (unwrap-uri *test-uri*)      ; download uri
                              (string->pointer "trek.txt") ; filename
                              50                           ; size
                              (time-rel #:milli 2)         ; eta
                              (time-rel #:seconds 1.3)     ; duration
                              50                           ; completed
                              0                            ; anonymity
                              0)                           ; is_active
                        12               ; GNUNET_FS_STATUS_DOWNLOAD_COMPLETED
                        %null-pointer)   ; filesharing handle
                  #:download #f))

(test-begin "test-fs-progress-info")

;; integer->progress-info-status
(test-equal '(#:unindex #:progress) (integer->progress-info-status 33))
(test-error 'invalid-arg (integer->progress-info-status 42))

;; progress-info-status->integer
(test-equal 8 (progress-info-status->integer '(#:download #:resume)))
(test-error 'invalid-arg (progress-info-status->integer
                          '(#:beam-me-up #:scotty)))

;; parse-c-progress-info
(define *test-pinfo*   (parse-c-progress-info *test-pinfo-ptr*))

(test-equal "trek.txt" (pinfo-download-filename *test-pinfo*))
(test-equal 50 (pinfo-download-size *test-pinfo*))
(test-equal #f         (pinfo-download-active?  *test-pinfo*))


;; bytevector-u8-fold
(let ((bv (make-bytevector 1)))
  (bytevector-u8-set! bv 0 #b01101010)
  (test-equal '(1 1 1 1)
              (bytevector-u8-fold (lambda (acc bool elt)
                                    (if bool
                                        (cons elt acc)
                                        acc))
                                    bv
                                    '(1 1 1 1 1 1 1 1)
                                    '())))

;; u8-bitmap->list
(let ((bv (make-bytevector 3)))
  (bytevector-u8-set! bv 0 #b11000001)
  (bytevector-u8-set! bv 1 #b00010100)
  (bytevector-u8-set! bv 2 #b00010000)
  (test-assert
      (lset= eq?
             '(#\T #\e #\r #\r #\y #\.)
             (u8-bitmap->list bv 0
                              '(#\T #\e #\a #\. #\ #\E #\a #\r
                                #\l #\- #\G #\r #\a #\y #\. #\ 
                                #\H #\o #\t #\.)))))

(test-end)
