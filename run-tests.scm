#!/usr/bin/guile \
-L . -s
!#
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define scandir (@ (ice-9 ftw) scandir))
(define (scm-file? f) (string-suffix? ".scm" f))

(define %test-directory "tests/")
(define %test-source-files (scandir %test-directory scm-file?))

(map load
     (map (lambda (f) (string-append %test-directory f))
	  %test-source-files))
